/*
 * io.c
 *
 *  Created on: 24 maj 2020
 *      Author: ciame
 */
#include "io.h"
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "../main.h"

uint8_t EEMEM adr1 = 0x00;
uint8_t EEMEM adr2 = 0x01;

uint8_t switch_state = off;
uint8_t level;

TIM Timer;

void pin_init (void)
{
	SWITCH_DIR &= ~SWITCH_PIN;
	SWITCH_PORT |= SWITCH_PIN;

	ON_OFF_DIR |= ON_OFF_PIN;
	POWER_ON;

	//Set all not used pins as inputs with pull up resistor
	DDRA &= ~((1<<PA2) | (1<<PA3) | (1<<PA4) | (1<<PA5) | (1<<PA6) | (1<<PA7));
	PORTA |= ((1<<PA2) | (1<<PA3) | (1<<PA4) | (1<<PA5) | (1<<PA6) | (1<<PA7));

	DDRB &= ~((1<<PB0) | (1<<PB1) | (1<<PB2) | (1<<PB3));
	PORTB |= ((1<<PB0) | (1<<PB1) | (1<<PB2) | (1<<PB3));

	DDRC &= 0x00;
	PORTC |= 0xFF;

	DDRD &= ~((1<<PD3) | (1<<PD6) | (1<<PD7));
	PORTD |= ((1<<PD3) | (1<<PD6) | (1<<PD7));

	#if DEBUG == 0
	DDRD |= (1<<PD1);
	PORTD |= (1<<PD1);

	LED_DIR |= LED_PIN;
	LED_PORT |= LED_PIN;
	#endif

	ATOMIC_BLOCK(ATOMIC_FORCEON)
	{
		level = eeprom_read_byte(&adr1);
		switch_state = eeprom_read_byte(&adr2);
	}
}

void super_debounce( volatile uint8_t *KPIN,
		uint8_t key_mask, uint16_t rep_time, uint16_t rep_wait,
		void (*push_proc)(void), void (*rep_proc)(void) )
{

	enum {idle, debounce, go_rep, wait_rep, rep};

	if(!rep_time) rep_time = 500;
	if(!rep_wait) rep_wait = 150;
	static uint8_t key_state = 0;
	uint8_t key_press = !(*KPIN & key_mask);

	if( key_press && !key_state ) {
		key_state = debounce;
		Timer.key = 8;
	}
	else if( key_state  )
	{

		if( key_press && debounce==key_state && !Timer.key ) {
			key_state = 2;
			Timer.key = 5;
		} else
		if( !key_press && key_state>1 && key_state<4 ) {
			if(push_proc) push_proc();						/* KEY_UP */
			key_state=idle;
		} else
		if( key_press && go_rep==key_state && !Timer.key ) {
			key_state = wait_rep;
			Timer.key = rep_wait;
		} else
		if( key_press && wait_rep==key_state && !Timer.key ) {
			key_state = rep;
		} else
		if( key_press && rep==key_state && !Timer.key ) {
			Timer.key = rep_time;
			if(rep_proc) rep_proc();						/* KEY_REP */
		}
	}
	if( key_state>=3 && !key_press ) key_state = idle;
}

void turn_on_off_lamp (void)
{
	if(switch_state) switch_state = off;
	else switch_state = on;
	Timer.working = WORK_TIME;
	ATOMIC_BLOCK(ATOMIC_FORCEON)
	{
		eeprom_write_byte(&adr2, switch_state);
	}

}

void change_level (void)
{
	if(++level > 3) level = low;
	Timer.working = WORK_TIME;
	ATOMIC_BLOCK(ATOMIC_FORCEON)
	{
		eeprom_write_byte(&adr1, level);
	}
}

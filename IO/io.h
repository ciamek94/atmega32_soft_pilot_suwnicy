/*
 * io.h
 *
 *  Created on: 24 maj 2020
 *      Author: ciame
 */

#ifndef IO_IO_H_
#define IO_IO_H_

#include <avr/io.h>
#include <avr/eeprom.h>


#define SWITCH_PIN_REGISTER PIND
#define SWITCH_DIR DDRD
#define SWITCH_PORT PORTD
#define SWITCH_PIN (1<<PD5)

#define ON_OFF_DIR DDRD
#define ON_OFF_PORT PORTD
#define ON_OFF_PIN (1<<PD4)
#define POWER_ON ON_OFF_PORT |= ON_OFF_PIN
#define POWER_OFF ON_OFF_PORT &= ~ON_OFF_PIN

#define LED_DIR DDRD
#define LED_PORT PORTD
#define LED_PIN (1<<PD0)
#define LED_ON LED_PORT |= LED_PIN
#define LED_OFF LED_PORT &= ~LED_PIN
#define LED_TOGGLE LED_PORT ^= LED_PIN

#define ZERO_REFERENCE 510

typedef struct
{
	uint8_t volatile watchdog;
	uint8_t volatile uart;
	uint8_t volatile mrf24j;
	uint16_t volatile key;
	uint16_t volatile working;
	uint16_t volatile led;
} TIM;

enum {low = 0, medium = 1, fast = 2, very_fast = 3, off = 0, on = 1};

extern uint8_t switch_state, level;
extern TIM Timer;

extern uint8_t EEMEM adr1;
extern uint8_t EEMEM adr2;

void pin_init (void);
void change_level (void);
void turn_on_off_lamp (void);

void super_debounce( volatile uint8_t *KPIN,
		uint8_t key_mask, uint16_t rep_time, uint16_t rep_wait,
		void (*push_proc)(void), void (*rep_proc)(void) );

#endif /* IO_IO_H_ */

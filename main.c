/*
 * main.c
 *
 *  Created on: 19 maj 2020
 *      Author: ciame
 */
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <util/atomic.h>
#include <avr/eeprom.h>

#include "MRF24J/mrf24j.h"
#include "ADC/adc.h"
#include "IO/io.h"
#include "main.h"


#if DEBUG == 1
#include "MKUART/mkuart.h"
#endif


volatile short int scaled_speed;

int main (void)
{
	wdt_enable(WDTO_120MS);

	#if DEBUG == 1
	USART_Init( __UBRR );			// inicjalizacja UART
	#endif

	wdt_reset();

	pin_init();
	init_INT0();
	init_Timer0();
	init_adc();

	sei();

	//Init MRF24j
	wdt_reset();
	mrf24j_SPI_init();
	mrf24j_reset();
	mrf24j_init();
	mrf24j_set_pan(PAN_ADDRESS);
	mrf24j_address16_write(REMOTE_CONTROL_ADREESS);
	wdt_reset();

	#if DEBUG == 1
	uart_puts("Init ");
	#endif

	Timer.working = WORK_TIME;

	while(1)
	{
		reset_watchdog_counter();
		mrf24j_send_message();

		#if DEBUG == 1
		send_data_by_uart();
		#endif

		super_debounce(&SWITCH_PIN_REGISTER, SWITCH_PIN, 0, 0, change_level, turn_on_off_lamp);
		led_toggle();

	}
}

void init_INT0 (void)
{
	DDRD &= ~(1<<PD2);
	PORTD |= (1<<PD2);
	MCUCR |= (1<<ISC01);
	GICR |= (1<<INT0);
}

void init_Timer0 (void)
{
	TCCR0 |= (1<<WGM01) | (1<<CS02) | (1<<CS00);
	TIMSK |= (1<<OCIE0);
	OCR0 = 77;
}

void reset_watchdog_counter (void)
{
	if (!Timer.watchdog)
	{
		Timer.watchdog = WATCHDOG_RESET_TIME;
		wdt_reset();
	}
}

void led_toggle (void)
{
	if(!Timer.led)
	{
		Timer.led = LED_TIME / (level + 1);
		LED_TOGGLE;
	}
}

void mrf24j_send_message (void)
{
	if (!Timer.mrf24j)
	{
		uint8_t message[] = {switch_state, level, scaled_speed >> 8, scaled_speed & 0xff};
		mrf24j_send(CRANE_CONTROLLER_ADDRESS, message ,sizeof(message));
		Timer.mrf24j = MRF24J_SEND_MESSAGE_DELAY;
	}
}
#if DEBUG == 1
void send_data_by_uart (void)
{
	if (!Timer.uart)
	{
		Timer.uart = 10;
		uart_putint(switch_state, 10);
		uart_puts("  ");
		uart_putint(level, 10);
		uart_puts("  ");
		uart_putint(scaled_speed, 10);
		uart_puts("\r\n");

	}
}
#endif


ISR (INT0_vect)
{
	mrf24j_interrupt_handler(&rx_info, &tx_info);
}

ISR (TIMER0_COMP_vect)
{

	uint16_t n;

#if DEBUG == 1
	n = Timer.uart;		/* 100Hz Timer */
	if (n) Timer.uart = --n;
#endif

	n = Timer.led;		/* 100Hz Timer */
	if (n) Timer.led = --n;
	n = Timer.watchdog;		/* 100Hz Timer */
	if (n) Timer.watchdog = --n;
	n = Timer.mrf24j;		/* 100Hz Timer */
	if (n) Timer.mrf24j = --n;
	n = Timer.key;		/* 100Hz Timer */
	if (n) Timer.key = --n;
	n = Timer.working;		/* 100Hz Timer */
	if (n)
	{
		Timer.working = --n;
		if (!Timer.working) POWER_OFF;
	}

	measure_speed = adc_measure(CHANNEL_1,measure_speed_array, DT_SPEED);
	scaled_speed = ((short int) measure_speed - ZERO_REFERENCE);
	if ( scaled_speed > 500) scaled_speed = 511;
	else if (scaled_speed < -500) scaled_speed = -511;
	if ( scaled_speed > 100 || scaled_speed < -100) Timer.working = WORK_TIME; //Reset controller work timer
}



#ifndef ADC_ADC_H_
#define ADC_ADC_H_

#define CHANNEL_0 0
#define CHANNEL_1 1
#define DT_SPEED 20


extern uint16_t volatile measure_speed;
extern uint16_t volatile measure_speed_array[DT_SPEED];

void init_adc (void);
uint16_t adc_measure( uint8_t channel, volatile uint16_t * mean_value, uint8_t  dt_parameter);
#endif /* ADC_ADC_H_ */

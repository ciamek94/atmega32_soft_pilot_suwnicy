/*
 * adc.c
 *
 *  Created on: 24 lut 2019
 *      Author: ciame
 */
#include <avr/io.h>
#include "adc.h"

uint16_t volatile measure_speed;
uint16_t volatile measure_speed_array[DT_SPEED];

//Inicjalizacja ADC
void init_adc (void)
{
	ADCSRA |= (1<<ADEN); // w��cz ADC
	ADCSRA |= (1<<ADPS2)|(1<<ADPS1);//|(1<<ADPS0); // preskaler
	ADMUX |= (1<<REFS0);	// 5V
}

//Funkcja dokonuj�ca pomiaru na wybranym kanale ADC
uint16_t adc_measure( uint8_t channel, volatile uint16_t * mean_value, uint8_t  dt_parameter)
{
	uint16_t return_val = 0;
	ADMUX = (ADMUX & 0b11111000) | channel;

	ADCSRA |= (1<<ADSC);  // start konwersji

	while( ADCSRA & (1<<ADSC) );

	for (uint8_t k = 1; k < DT_SPEED; k++) return_val +=  *(mean_value + k);

	return_val = return_val + ADCW;
	*mean_value = ADCW;

	for (uint8_t k = DT_SPEED - 1; k > 0; k--) *(mean_value + k) = *(mean_value + k - 1);

	return_val = return_val / dt_parameter ;

	return return_val;
}

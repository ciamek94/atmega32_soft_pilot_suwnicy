/*
 * main.h
 *
 *  Created on: 8 lut 2021
 *      Author: ciame
 */

#ifndef MAIN_H_
#define MAIN_H_
#include "IO/io.h"

#define DEBUG 0

#define WATCHDOG_RESET_TIME 5
#define MRF24J_SEND_MESSAGE_DELAY 20
#define WORK_TIME 3000
#define LED_TIME 300

void init_INT0 (void);
void init_Timer0 (void);
void reset_watchdog_counter (void);
void led_toggle (void);
void mrf24j_send_message (void);

#if DEBUG == 1
void send_data_by_uart (void);
#endif

extern TIM Timer;

#endif /* MAIN_H_ */

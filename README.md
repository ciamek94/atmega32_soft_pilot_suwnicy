# Pilot suwnicy

## Opis działania

Zaprojektowany układ elektroniczny służy do sterowania pracą suwnicy (dźwigu). Pilot służy do sterowania kierunkiem pracy napędu bezprzewodowo.

Układ zasilany jest z baterii 9V 6LR61. Układ oparty jest na mikrokontrolerze AVR Atmega32. Przesył danych pomiędzy pilotem, a sterownikiem realizowany jest z wykorzystaniem modułu frmy Microchip MRF24j40ma pracującym w paśmie częstotliwości 2,4GHz w standardze IEEE 802.15.4 (Zigbee). Ramka danych zawiera m.in. dane na temat kierunku, prędkości zadanej, stanu dodatkowego przekaźnika sterującego pracą oświetlenia. Ramka wysyłana jest co pewien okres zdefiniowany w programie. Brak otrzymania ramki z pilota przez sterownik powoduje zatrzymanie napędu.

Na płytce umieszczona jest przetwornica PWM napięcia przetwarzająca napięcie 9V na napięcie zasilające mikrokontoroler oraz moduł radiowy MRF24j40ma. W programie zimplementowano programowy timer odliczający czas, gdy urządzenie jest nieaktywne(joistick jest w położeniu neutralnym). Po jego upływie następuje samoczynne wyłączenie pilota co pozwala na oszczędność energii.

Użytkownik steruje pracą napędu (suwnicy) przesuwając dźwignię joysticka zamontowanego w pilocie do góry lub na dół. Mikrokontroler realizuje pomiar napięcie na dzielniku napięcia (joystick), którego rezystancja ulega zmianie w zależności od aktualnego położenia, a w konsekwencji napięcie jakie mierzy mikrokontroler. Joystick posiada przycisk, który pozwala na przesuwanie po poszczególnych zakresach prędkości, których w układzie zdefiniowano cztery. W zależności od wybranego poziomu dioda led miga z różną częstotliwością.

![](./remote_control_images/IMG_20210425_182624590.jpg )
![](./remote_control_images/IMG_20210425_182634869.jpg )
![](./remote_control_images/IMG_20210425_182754064.jpg )
![](./remote_control_images/IMG_20210425_182802859.jpg )
![](./remote_control_images/IMG_20210425_182818984.jpg )
![](./remote_control_images/IMG_20210425_182923903.jpg )


 
